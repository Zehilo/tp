export default interface IInterval {
    start: string
    end: string
}

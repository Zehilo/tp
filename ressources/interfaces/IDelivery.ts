import IContact from "./IContact";
import ICarrier from "./ICarrier";
import IInterval from "./IInterval";

export default interface IDelivery {
    storePickingInterval: IInterval
    deliveryInterval: IInterval
    contact: IContact
    carrier: ICarrier
}

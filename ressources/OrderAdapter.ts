import Order from "./Order";
import IPackage from "./interfaces/IPackage";
import IDelivery from "./interfaces/IDelivery";

export default class OrderAdapter extends Order {

    constructor(id: number, packages: [IPackage], delivery: IDelivery) {
        super(id, packages, delivery);
        this.setAnonymInfo()
    }

    setAnonymInfo() {
        this.delivery.contact.address = "********"
        this.delivery.contact.city = "********"
        this.delivery.contact.email = "********"
        this.delivery.contact.fullname = "********"
        this.delivery.contact.phone = "********"
        this.delivery.contact.postalCode = "********"
    }
}

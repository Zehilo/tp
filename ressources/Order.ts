import IOrder from "./interfaces/IOrder";
import IPackage from "./interfaces/IPackage";
import IDelivery from "./interfaces/IDelivery";

export default class Order implements IOrder {

    id: number;
    packages: [IPackage];
    delivery: IDelivery;

    constructor(id: number, packages: [IPackage], delivery: IDelivery) {
        this.id = id;
        this.packages = packages
        this.delivery = delivery
    }
}

import IMeasure from "./IMeasure";
import IProduct from "./IProduct";

export default interface IPackage {
    length: IMeasure
    width: IMeasure
    height: IMeasure
    weight: IMeasure

    products: IProduct[]

}

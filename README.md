#TP_POA CANTA Carmelo

#Question 1
J'ai pour ma part créé 2 fichiers (App.ts & Server.ts)
Je crée un nouveau serveur dans mon fichier www.ts en renseignant le port désiré.

App est actuellement un singleton.

PATH -> services/

#Question 2
Le pattern d'architecture adapté à cette situation est :
Le modele MVC (Modèle-vue-contrôleur)

En suivant ce modèle les actions présentent dans mes routes 
appellent maintenant mes fonctions présentent dans les controllers.

J'ai pour cela créé 2 controleurs avec des fonctions static: \
PATH -> controllers/

-> IndexController.ts appelé par index.ts \
-> RouterController.ts appelé par orders.ts

#Question 3
Pour cette question j'ai ajouté différentes interfaces présentent dans le dossier suivant : \
    ressources/interfaces \
Ces interfaces me permettent de créer un objet 'Order' et de vérifier que mon objet dans _order.ts est bien de ce type. \
Pour le principe utilisé de l'acronime SOLID il s'agit du I (Interface segregation principle) soit Ségrégation des interfaces en français.

#Question 4
Pour cette question le design patern utilisé est le patron de conception "Adaptateur"
Comme son nom l'indique il va me permettre d'adapter un Order en un Order avec un contact anonmyme.

J'ai donc créé une classe 'OrderAdapter' qui extends un 'Order' et dans son constructeur je rend anonyme mon contact.
Je tranforme mes orders en orders anonym au moment d'envoyer le JSON.

#Question 5
Comme demandé j'ai créé une classe 'Storage' qui va initialiser un storage avec la value donné en paramètre.

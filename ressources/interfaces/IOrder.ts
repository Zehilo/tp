import IDelivery from "./IDelivery";
import IPackage from "./IPackage";

export default interface IOrder {
    id: number;
    packages: IPackage[]
    delivery: IDelivery
}

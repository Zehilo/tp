import express, {Express} from 'express'
import logger from "morgan";
import cookieParser from "cookie-parser";
import path from "path";
import {orders} from "../data/_orders";
import indexRouter from "../routes";
import ordersRouter from "../routes/orders";
import Storage from "../ressources/Storage";

export default class App {

    private static instance: Express;

    private constructor(port: number) {

        App.instance = express()

        this.registerMiddlewares()
        this.registerRoutes()
        this.initDefaultData()

        App.instance.set('port', port)

    }

    private registerMiddlewares() {
        App.instance.use(logger('dev'))
        App.instance.use(express.json())
        App.instance.use(express.urlencoded({extended: false}))
        App.instance.use(cookieParser())
        App.instance.use(express.static(path.join(__dirname, 'public')))
    }

    private registerRoutes() {
        App.instance.use('/', indexRouter)
        App.instance.use('/orders', ordersRouter)
    }

    // Init default data
    private initDefaultData() {
        new Storage('orders')
    }

    public static getInstance(port: number): Express {
        if (!App.instance) {
            new App(port);
        }

        return App.instance;
    }

}

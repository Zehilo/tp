import storage, {DatumOptions, InitOptions, WriteFileResult} from "node-persist";
import {orders} from "../data/_orders";

export default class Storage {

    static dataType: string

    constructor(type: string) {
        Storage.dataType = type
        Storage.initializeDefaultData()
    }

    static initializeDefaultData(options?: InitOptions) {
        let values: any
        switch (Storage.dataType) {
            case 'orders':
                values = orders
                break
            case 'toto':
                //values = toto
                break
        }
        return storage.init(options).then(() => {
            Storage.setItem(Storage.dataType, values)
        })
    }

    static getItem(type: string): Promise<any> {
        return storage.getItem(type)
    }

    static setItem(key: string, value: any, options?: DatumOptions): Promise<WriteFileResult> {
        return storage.setItem(key, value, options)
    }

}

export default interface ICurrency {
    currency: string
    value: number
}

export default interface IContact {
    fullname: string
    email: string
    phone: string
    address: string
    postalCode: string
    city: string
}

import {Request, Response, Router} from 'express'
import {RouterController} from "../controllers/RouterController";

const router = Router()

router.get('/', async (req: Request, res: Response) => {
    await RouterController.getOrders(req, res)
})

router.get('/:id', async (req: Request, res: Response) => {
    await RouterController.getOrderByID(req, res)
})

router.post('/', async (req: Request, res: Response) => {
    await RouterController.postOrder(req, res)
})

export default router

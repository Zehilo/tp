import ICurrency from "./ICurrency";

export default interface IProduct {
    name: string
    price: ICurrency
}
